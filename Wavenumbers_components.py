import numpy as np
import matplotlib.pyplot as plt
import scipy.spatial as spatial
from scipy import stats as stats

#####################################################################
# Plot functions
#####################################################################

# Plot the x and z components of the wavenumbers
def plot_scatter(points_cloud, labels, **kwargs):
    """
    Plot points in the plane
    """
    plt.figure(figsize=(20, 20))
    ax = plt.gca()
    ax.set_aspect('equal', adjustable = 'box')    # aspect ratio is 1
    plt.xlabel("$k_x$", fontsize = 'xx-large')
    plt.ylabel("$k_z$", fontsize ='xx-large')

    # Input is a list
    if type(points_cloud) is list:
        N = len(points_cloud) # number of sets 
        colors = ["#1f77b4"]*N # Default color: steelblue
        markers = ["o"]*N
        if 'colors' in kwargs.keys():
            colors = kwargs['colors']
        if 'markers' in kwargs.keys():
            markers = kwargs['markers']
        for i in range(N):
            X, Y = flatten_wvnb_matrix(points_cloud[i])
            marker = markers[i]
            color = colors[i]
            label = labels[i]
            ax.scatter(X, Y, marker = marker, color = color, label = label)
    # Input is a unique cloud of points
    elif isinstance(points_cloud, np.ndarray):
        color = "#1f77b4" # Default color: steelblue
        marker = "o"
        if 'colors' in kwargs.keys():
            color = kwargs['colors']
        if 'markers' in kwargs.keys():
            marker = kwargs['markers']
        X, Y = flatten_wvnb_matrix(points_cloud)
        ax.scatter(X, Y, marker = marker, color = color, label = labels)

    if 'title' in kwargs.keys():
        plt.title(kwargs['title'])
    plt.legend()
    plt.show()

# Plot the disposition of the acquisition installation (location of the sources and receivers)
def plot_disposition(S, R, x, **kwargs):
    """
    Plot points in the plane
    """ 
    plt.figure(figsize=(20, 10))
    ax = plt.gca()
    ax.set_aspect('equal', adjustable = 'box')    # aspect ratio is 1
    plt.xlabel("$x$", fontsize = 'x-large')
    plt.ylabel("$z$", fontsize ='x-large')
    ax.scatter(S[:,0], S[:,1], marker = "v", label = "sources")
    ax.scatter(R[:,0], R[:,1], marker = "^", label = "receivers")
    ax.scatter(x[0], x[1], marker = "X", label = "target point")
    if 'limits' in kwargs.keys():
        limits = kwargs['limits']
        ax.scatter(limits[:,0], limits[:,1], marker = "x", color = "r", label = "boundary of the acquisition range")
    if 'title' in kwargs.keys():
        plt.title(kwargs['title'])
    plt.legend()
    plt.show()
    
# A function that allows to vizualize the nearest neighbour of a point and compare it to the neighbours in the indices space
# Input:
# K: Wavenumber cloud of points
# K_unique: Wavenumber cloud of points with no duplicated points (returned by the function find_nearest_neighbours)
# index: index of the point of interest
# List_true_nn : full list of the nearest neighbour indices associated to the points in K (returned by the function find_nearest_neighbours)
# List_neighbours_indices: list of the points that are neighbours in the indices space (returned by function min_indices_distance)
# ! nn: nearest neighbour (abbrev.)
def plot_wvnb_pts_nearest_neighbours(K, K_unique, index, List_true_nn, List_neighbours_indices):
    index_true_nn = List_true_nn[index] # What is the index in K_unique of the nearest neighbour?
    nn = K_unique[index_true_nn,:] # What is the point associted to the nearest neighbour?
    List = [K, K[index,:].reshape(1,2) , List_neighbours_indices[index], nn.reshape(1,2)] # List of set of points
    plot_scatter(List, colors = ['steelblue', 'orange', 'darkred', 'limegreen'], labels = ["Wavenumbers", 'Point of index ' + str(index), 'Adjacenmt points in the indices', 'Found nearest neighbour'], markers = ['.', 'd', 'P', 'x'], title = "Point of index " + str(index) + " : its nearest neighbour and adjacent points in index space")

# A function to get the arrays of x and z components in a wavenumbers matrix
def flatten_wvnb_matrix(K):
    Kx = K[:,0]
    Kz = K[:,1]

    return Kx, Kz

#####################################################################
# Wavenumber functions
#####################################################################

#Compute wavenumber components using formula 3.125: k = k0(ps + pr)
def wavenumber_components125(x, s, r, c0, omega):
    e2 = np.array([0, 1])

    ps = x - s
    ps = ps/np.linalg.norm(ps)  # unit vector in the direction of the rays connecting x to s
    pr = x - r
    pr = pr/np.linalg.norm(pr) # unit vector in the direction of the rays connecting x to r

    k0 = (2*np.pi*omega)/c0

    k = k0*( ps + pr ) # formula 3.125

    return k

#Compute wavenumber components using formula 3.132: k = 2*k_0 cos( theta/2 ) (cos phi, sin phi)
def wavenumber_components132(x, s, r, c0, omega):
    e2 = np.array([0, -1])

    ps = x - s
    ps = ps/np.linalg.norm(ps)  # unit vector in the direction of the rays connecting x to s
    pr = x - r
    pr = pr/np.linalg.norm(pr) # unit vector in the direction of the rays connecting x to r

    k0 = (2*np.pi*omega)/c0
    
    # all the vectors are unit vectors so no need to divide by the norms
    phis = np.arccos(np.inner(ps, e2)) # angle made by ps with the vertical, i.e. vector (0, -1)
    phir = np.arccos(np.inner(pr, e2)) # angle made by ps with the vertical, i.e. vector (0, -1)

    phi = phis - phir 

    # We compute cos of half the angle between ps and pr
    cos_theta = np.inner(ps, pr) # cos( theta )
    cos_half_theta = np.sqrt( 0.5*(cos_theta + 1.)) # cos( theta/2)

    # We compute the wavenumbers components,
    phi_polar_coordinates = phi - np.pi/2.
    k = 2*k0*cos_half_theta*np.array([np.cos(phi_polar_coordinates), np.sin(phi_polar_coordinates) ])
        
    return k

# k = 2*k_0 cos( (phis - phir)/2 ) (cos phi, sin phi)
def wavenumber_components132_modified(x, s, r, c0, omega):
    e2 = np.array([0, -1]) # the reference direction

    ps = x - s
    ps = ps/np.linalg.norm(ps)  # unit vector in the direction of the rays connecting x to s
    pr = x - r
    pr = pr/np.linalg.norm(pr) # unit vector in the direction of the rays connecting x to r

    k0 = (2*np.pi*omega)/c0
    
    # We check if the angles of ps and pr are negative or positive compared to the reference direction
    # before computing them
    # all the vectors are unit vectors so no need to divide by the norms
    if( ps[0] >= e2[0]):
        phis = np.arccos(np.inner(ps, e2)) # angle made by ps with the vertical, i.e. vector (0, -1)
    else:
        phis = - np.arccos(np.inner(ps, e2))
    
    if( pr[0] >= e2[0]):
        phir = np.arccos(np.inner(pr, e2)) # angle made by ps with the vertical, i.e. vector (0, -1)
    else:
        phir = - np.arccos(np.inner(pr, e2)) # angle made by pr with the vertical, i.e. vector (0, -1)
    
    # We compute the wavenumbers components 
    k = wavenumber_angular_formula(phis, phir, k0)
        
    return k
        
"""
method: corresponds to the equation used, either 3.125 or 3.132 in the course, and to the 3.132 modified 
"""
def wavenumbers_components(x, S, R, c0, f0, method = "125"):
    #assert method == "125" or method == "132", "Error: choose either \"125\" or \"132\" to pass to method; these correspond to the equations in the 3rd chapter of the course"
    K = [] # wavenumbers
    omega = f0
    
    # compute the wavenumber for each couple of source and receiver
    for s in S: 
        for r in R: 
            if(method == "125"):
                k = wavenumber_components125(x, s, r, c0, omega)
            elif(method == "132"):
                k = wavenumber_components132(x, s, r, c0, omega)
            else:
                k = wavenumber_components132_modified(x, s, r, c0, omega)
            K.append(k)
    
    K = np.array(K).reshape(S.shape[0]*R.shape[0], 2)
    return K

"""
Function that computes wavenumbers componenets using sets of angles phi_s and phi_r
"""
def wavenumbers_components_angles(phiS, phiR, c0, f0):
    K = [] # wavenumbers
    omega = f0
    k0 = 2*np.pi*omega/c0
    
    # compute the wavenumber for each couple of source and receiver angles
    for phis in phiS: 
        for phir in phiR: 
            # We use the angles formula
            k = wavenumber_angular_formula(phis, phir, k0)
            K.append(k)
    
    K = np.array(K).reshape(phiS.shape[0]*phiR.shape[0], 2)
    return K

#####################################################################
# Functions used in the notebook about parametrization of the wavenumbers cloud
#####################################################################

# Function that computes the components of a wavenumber  using the plane formula (cartesian coordinates of the sources and receivers)
def wavenumber_plane_formula(xs, xr, x, y, k0):
    norm_s = np.sqrt( (xs - x)*(xs - x) + y*y )
    norm_r = np.sqrt( (xr - x)*(xr - x) + y*y )
    kx = (x - xs)/norm_s + (x - xr)/norm_r 
    kz = ( 1/norm_s + 1/norm_r )*y

    return k0*kx, k0*kz

# Function that computes the components of a wavenumber using the angular formula (angles $\phi_s$ and $\phi_r$)
def wavenumber_angular_formula(phis, phir, k0):
    phi = (phis + phir)/2
    theta = phis - phir
    kx = 2*k0*np.cos(theta/2)*np.sin(phi)
    kz = - 2*k0*np.cos(theta/2)*np.cos(phi)

    return np.array([kx, kz])

# A function that extracts the values of the angles $\phi_s$ and $\phi_r$ from the layout of the sources S and receivers R
# x is the diffraction point
def layout2angles(S, R, x):
    e2 = np.array([0, -1]) # the reference direction

    phiS = []
    # Remark: all the vectors are unit vectors so no need to divide by the norms
    for s in S:
        ps = x - s
        ps = ps/np.linalg.norm(ps)  # unit vector in the direction of the rays connecting x to s
        # We check if the angle of ps is negative or positive compared to the reference direction
        # before computing it
        if( ps[0] >= e2[0]):
            phis = np.arccos(np.inner(ps, e2)) # angle made by ps with the vertical, i.e. vector (0, -1)
        else:
            phis = - np.arccos(np.inner(ps, e2))
        phiS.append(phis)
        
    phiR = []
    for r in R:
        pr = x - r
        pr = pr/np.linalg.norm(pr) # unit vector in the direction of the rays connecting x to r
        # We check if the angle of pr is negative or positive compared to the reference direction
        # before computing it
        if( pr[0] >= e2[0]):
            phir = np.arccos(np.inner(pr, e2)) # angle made by ps with the vertical, i.e. vector (0, -1)
        else:
            phir = - np.arccos(np.inner(pr, e2)) # angle made by pr with the vertical, i.e. vector (0, -1)
        phiR.append(phir)
    
    phiS = np.array(phiS)
    phiR = np.array(phiR)
    
    return phiS, phiR

# Function that generates wavenumbers components from angles $\phi_s$ and $\phi_r$ using the circular arc formulation of the wavenumbers cloud
def wavenumbers_circular_arcs(phiS, phiR, k0):
    # The wavenumbers 
    K = []
    # The circular arcs centers
    Centers = []
    for phis in phiS:
        # We first compute the center of teh circular arc
        center = k0*np.array([np.sin(phis), - np.cos(phis)])
        Centers.append(center)
        for phir in phiR: 
            # Computation of the x, z components
            k = center + k0*np.array([np.cos(phir - np.pi/2), np.sin(phir - np.pi/2)])
            K.append(k)
    K = np.array(K)
    Centers = np.array(Centers)

    # We return the centers too in case we need them
    return K, Centers

# A function that computes the layout of the sources S and receivers R from the angles phi_s and phi_r
# x is the diffraction point
def angles2layout(phiS, phiR, x):
    Sx = []
    Sz = np.zeros(phiS.shape[0]) # the sources will be situated on the surface
    for phis in phiS:
        sx = x[0] + x[1]*np.tan(phis)
        Sx.append(sx)
    
    Rx = []
    Rz = np.zeros(phiR.shape[0]) # the receivers will be situated on the surface
    for phir in phiR:
        rx = x[0] + x[1]*np.tan(phir)
        Rx.append(rx)
    S = np.array([Sx, Sz]).T
    R = np.array([Rx, Rz]).T
    
    return S, R

# colors: We will color the circular arcs to differentiate them
def compute_circular_arcs(phiS, phiR, k0, nb_arcs, colors):
    # The indices of the angles $\phi_s$ that we will fix to obtain its corresponding circular arc 
    fixed = np.linspace(start = 0, stop = phiS.shape[0] - 1, dtype = 'int', num = nb_arcs)
    
    # A list of the generated circular arcs and their associated labels
    Circular_Arcs = []
    Labels_arcs = []
    
    # A list of the centers of the circular arcs 
    Centers_Arcs = []
    Labels_centers = []
        
    for i in range(fixed.shape[0]):
        a = phiS[fixed[i]] # the fixed angle corresponding to phi_s
        color = colors[i] # The color of the circular arc
        center = k0*np.array([np.sin(a), - np.cos(a)]) # the center of the circular arc
        Circular_arc = [] # the list of the points in the arc
        for phir in phiR: 
            # the cartesian coordinates of the points in the arc
            k = center + k0*np.array( [np.cos(phir - np.pi/2), np.sin(phir - np.pi/2)])
            Circular_arc.append( k )
            
        Circular_Arcs.append(np.array(Circular_arc))
        a_degrees_str = str(0.001*np.trunc(np.rad2deg(a)*1000))[:6] # a string to print the fixed angle in degrees
        Labels_arcs.append('circular arc of angle ' + a_degrees_str + ' degrees')
        
        Centers_Arcs.append(center.reshape(1,2))
        Labels_centers.append('center of the circular arc')
        
    return Circular_Arcs, Centers_Arcs, Labels_arcs, Labels_centers

#####################################################################
# Functions for the distances
#####################################################################

# A function that computes for each point in the cloud the distance to its nearest neighbour, using a kd-tree, and then prints the histogram of the distances and their statistics
def find_nearest_neighbours(K):
    Kx, Kz = flatten_wvnb_matrix(K)
    K_linear = np.array([Kx, Kz])
    K_unique = np.unique(K_linear, axis = 1).T
    tree = spatial.KDTree(K_unique)

    distances, neighbours = tree.query(K_unique, k = 2)
    distances = distances[:,1]
    neighbours = neighbours[:,1]

    distances_full, neighbours_full = tree.query(K_linear.T, k = 2)
    distances_full = distances_full[:,1]
    neighbours_full = neighbours_full[:,1]

    return K_unique, distances, neighbours, distances_full, neighbours_full


def histogram_stats_distances(distances, K):

    statistics = stats.describe(distances)

    plt.figure( figsize = (10, 5))
    ax = plt.gca()
    ax.hist(distances, bins = 200)
    plt.title("Distribution of the distances between points and their nearest neighbour in the wavenumber cloud")
    
    
    nb_pts = statistics[0]
    d_min = statistics[1][0]
    d_max = statistics[1][1]
    d_mean = statistics[2]
    d_var = statistics[3]
    
    print("============================================")
    print("Statistics about the distances:")
    print("----------------")
    print("Number of points with the duplicates: ", K.shape[0])
    print("Number of points after supression of the duplicates: ", nb_pts)
    print("Minimum distance: ", d_min)
    print("Maximum distance: ", d_max)
    print("Mean distance: ", d_mean)
    print("variance of the distances: ", d_var)

    return statistics

# function that computes the distance between two wavenumber points by using their phis and phir angles
def compute_distance_increment_formula(phis, phir, phis_neighbour, phir_neighbour, k0):
    # The increnments in angles, respectively in the sources and the receivers space 
    a_s = phis_neighbour - phis
    a_r = phir_neighbour - phir 
    # The formula
    return 2*k0*np.sqrt( np.sin(a_s/2)**2 + np.sin(a_r/2)**2 + 2*np.sin(a_s/2)*np.sin(a_r/2)*np.cos(phis - phir + 0.5*(a_s - a_r)))

# A function that, for each point genereated by a couple of angles (phis, phir), computes the minimum of the distances
# to its 8 neighbours in the indices space, using two methods: 
# distances_euclidean: simply using the euclidean distance
# distances_formula: using the formula that gives the distance between two points using thei angles phi_s and phi_r
# and saves the points associated to those 8 neighbours
def min_indices_distance(phiS, phiR, k0):

    # empty lists that will store the minimum indice distance for each point in the wavenumber cloud 
    distances_formula = []
    distances_euclidean = []
    # Lists of the lists of neighbours
    Lists8 = []
    # we identify a point by its indices
    for i in range(phiS.shape[0]):
        for j in range(phiR.shape[0]):
            formula_d = [] # list of the distances to the indices neighbours using the formula
            euclidean_d = [] # ... using the euclidean distance
            List8 = [] # List of the indices neighbours of the point 
            
            # We compute the wavenumber point of interest
            phis = phiS[i]
            phir = phiR[j]
            q = wavenumber_angular_formula(phis, phir, k0)

            # we loop through the 8 neighbours; be careful not to get out of range
            for k in [np.max([i - 1, 0]), i, np.min([i + 1, phiS.shape[0] - 1])]:
                for l in [np.max([j - 1, 0]), j, np.min([j + 1, phiR.shape[0] - 1])]:
                    phis_neighbour = phiS[k] 
                    phir_neighbour = phiR[l] 
                    k_neighbour = wavenumber_angular_formula(phis_neighbour, phir_neighbour, k0)
                    # We make sure the enighbours is not the point itself ( happens when we have symmetric sources and receivers)
                    if np.all(q != k_neighbour) :
                        d = compute_distance_increment_formula(phis, phir, phis_neighbour, phir_neighbour, k0) # distance formula
                        formula_d.append(d) 
                        euclidean_d.append( np.linalg.norm(k_neighbour - q) ) # Euclidean distance
                        List8.append(k_neighbour) # the point associated to this neighbour
            
            # We keep the minimum of both lists of distances
            distances_formula.append(np.min(formula_d))
            distances_euclidean.append(np.min(euclidean_d))
            # We uniquely identify the points
            List8 = np.unique(List8, axis = 0)
            Lists8.append(np.array(List8))
            
    # We convert the lists to arrays to facilitate computations
    distances_euclidean = np.array(distances_euclidean)
    distances_formula = np.array(distances_formula)
    
    return distances_formula, distances_euclidean, Lists8
