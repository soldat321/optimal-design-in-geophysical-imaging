import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import Voronoi, voronoi_plot_2d
import matplotlib.path as mplPath
from Wavenumbers_components import *
from shapely.geometry import Polygon
from shapely.geometry import Point
from colorized_voronoi import voronoi_finite_polygons_2d

# array: flattened 2xn array
def reshape_into_matrix(array):
    return array.flatten().reshape( int(array.shape[0]/2), 2)

# seeds: flattened 2xn array
# ndim: number of dimensions of the array
def voronoi_domain(seeds, domain):
    # We construct the polygon of the domain
    polygon = Polygon(np.array(domain))
    
    # We keep only the seeds that are inside the domain
    # https://stackoverflow.com/questions/42305987/python-shapely-polygon-containspoint-not-giving-correct-answer-i-think
    # https://stackoverflow.com/questions/59417997/how-to-plot-a-list-of-shapely-points
    if seeds.ndim == 1:
        inside = reshape_into_matrix(seeds)
    else:
        inside = seeds
    #points = [Point(p) for p in inside] # Shapely points
    #is_inside = [polygon.contains(p) for p in points]
    #inside = inside[is_inside]
    
    # We construct the unbounded Voronoi diagram from seeds
    vor = Voronoi(inside)
    
    # Copyright: Pauli Virtanen 
    # https://gist.github.com/pv/8036995
    # TO DO: find an alternative to assigning a large value to radius in order to avoid voronoi cells not 
    # forming a partition of the domain
    regions, vertices = voronoi_finite_polygons_2d(vor, radius = polygon.length*100)

    new_regions = []
    for region in regions:
        polygon_cell = Polygon(vertices[region])
        intersection = polygon.intersection(polygon_cell)
        # Get rid of th geom_type test; l-bfgs-b often gives seeds outside of the domain, so the intersection will be empty
        # if there is no test the program crashes because "GeometryCollection" has no exterior
        # so if the seed is outside (intersection is empty) we take the whole cell (the vertices of which are far far away)
        # and we will get a very large cost (value of objective fct) and this will "punish" l-bfgs-b from choosing 
        # a seed outside the domain and at the next iteration it won't do it again
        if intersection.geom_type == "GeometryCollection":
            new_regions.append(np.asarray(polygon_cell.exterior.coords))
        else:
            new_regions.append(np.asarray(intersection.exterior.coords))
    
    return vor.points, new_regions

# seeds: 2xn array
def plot_voronoi_domain(seeds, domain, cells):
    
    plt.figure(figsize = (20, 20))
    ax = plt.gca()
    ax.set_aspect('equal')
    
    # We construct the polygon of the domain
    # https://stackoverflow.com/questions/55522395/how-do-i-plot-shapely-polygons-and-objects-using-matplotlib
    polygon = Polygon(np.array(domain))
    ax.plot(*polygon.exterior.xy, 'k-', label = "domain boundary")
    
    centroids = np.zeros((len(cells), 2))
    i = 0
    for cell in cells:
        polygon_cell = Polygon(cell)
        ax.fill(*polygon_cell.exterior.xy, alpha = .8)
        centroids[i, :] = np.asarray(polygon_cell.centroid)
        i = i + 1
    ax.plot(centroids[:,0], centroids[:,1], 'rx', label = "centroids")
    
    ax.plot(seeds[:,0], seeds[:,1], 'kP', label = "seeds")
    
    plt.legend()
    plt.show()    

def Lloyd(initial_seeds, domain, display = False):
    seeds, regions = voronoi_domain(initial_seeds, domain)
    centroids = []
    iterations = 0
    for region in regions:
            centroid = np.array( Polygon(region).centroid )
            centroids.append(centroid)
    centroids = np.array(centroids)
    min_norm = np.min( np.linalg.norm(centroids - seeds, axis = 1) )
    while (( min_norm > 10**(-6) ) & (iterations < 1000)) :
        iterations = iterations + 1
        seeds, regions = voronoi_domain(centroids, domain)
        centroids = []
        for region in regions:
                centroid = np.array( Polygon(region).centroid )
                centroids.append(centroid)
        centroids = np.array(centroids)
        min_norm = np.min( np.linalg.norm(centroids - seeds, axis = 1) )
    if display:
        return seeds, regions, iterations
    else:
        return seeds, regions

def domain_construction(phi_limits, k0):
    # The sides circular arcs
    angles_centers = phi_limits
    angles_arcs = np.linspace(start = phi_limits[0], stop = phi_limits[1], num = 100, dtype = float)
    colors = ['r', 'g'] # enpty colors; no need for them
    Arcs, Centers, A_labels, C_labels = compute_circular_arcs(angles_centers, angles_arcs, k0, angles_centers.shape[0], colors)

    # The bottom circular arcs
    Superimposed = []
    for phis in angles_arcs:
        phir = phis
        k = wavenumber_angular_formula(phis, phir, k0)
        Superimposed.append(k)
    Superimposed = np.array(Superimposed)  

    A = np.append(Arcs[0][:-1,:], Arcs[1], axis = 0) # No intersection between the two sides arcs
    B = np.flip(Superimposed, axis = 0)[1:-1,] # No intersection between the sides arcs and the borrom arc

    # We get rid og the two side horns tips to get a valid polygon (and be able to perfom intersections with other polygons) 
    # https://stackoverflow.com/questions/13062334/polygon-intersection-error-in-shapely-shapely-geos-topologicalerror-the-opera
    #Domain_invalid = np.append(A, B, axis = 0)
    #Polygon(np.array(Domain_invalid)).is_valid
    return np.append(A[1:-1,:], B, axis = 0)

def half_disk_domain_construction(phi_limits, k0):
    # The sides circular arcs
    angles_arcs = np.linspace(start = phi_limits[0], stop = phi_limits[1], num = 100, dtype = float)
    colors = ['r', 'g'] # enpty colors; no need for them
    

    # The bottom circular arcs
    Superimposed = []
    for phis in angles_arcs:
        phir = phis
        k = wavenumber_angular_formula(phis, phir, 1.05*k0)
        Superimposed.append(k)
    Superimposed = np.array(Superimposed)  

    top = wavenumber_angular_formula(phi_limits[0], phi_limits[1], k0)
    top = np.array([0, -1])
   
    return Superimposed
