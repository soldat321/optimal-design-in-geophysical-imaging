import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Voronoi, voronoi_plot_2d
import matplotlib.path as mplPath
from shapely.geometry import Polygon
from shapely.geometry import Point
import quadpy
from cvt import *
from cvt_bfgs import *
from Wavenumbers_components import *

def Phi_extraction(Phi, m):
    PhiS = Phi[:m]
    PhiR = Phi[m:]
    return PhiS, PhiR

def FoK(Phi, domain, c0, f0, m):
    PhiS, PhiR = Phi_extraction(Phi, m)
    K = wavenumbers_components_angles(PhiS, PhiR, c0, f0)
    return F(K, domain)

def jacobian_K(PhiS, PhiR, c0, f0):
    k0 = 2*np.pi*f0/c0
    m = PhiS.shape[0]
    n = PhiR.shape[0]
    J = np.zeros((2*m*n, m + n))
    for i in range(m):
        for j in range(n):
            J[i*2*n + 2*j, i] = np.cos(PhiS[i])
            J[i*2*n + 2*j, m + j] = np.cos(PhiR[j])
            
            J[i*2*n + 2*j + 1, i] = np.sin(PhiS[i])
            J[i*2*n + 2*j + 1, m + j] = np.sin(PhiR[j])
            
    J = k0*J
    return J
            
def grad_FoK(Phi, domain, c0, f0, m):
    PhiS, PhiR = Phi_extraction(Phi, m)
    K = wavenumbers_components_angles(PhiS, PhiR, c0, f0)
    gradF = grad_F(K, domain) 
    jacobianK = jacobian_K(PhiS, PhiR, c0, f0)
    return np.dot(jacobianK.T, gradF)

def plot_optimization_result(X, m, limits, x_diffraction_pt, c0, f0):
    PhiS, PhiR = Phi_extraction(X, m)
    S, R = angles2layout(PhiS, PhiR, x_diffraction_pt)
    K = wavenumbers_components(x_diffraction_pt, S, R, c0, f0, method = "132_modified")
    plot_scatter(K, "Wavenumbers ", title = "Result of the optimization.", color = 'steelblue')
    plot_disposition(S, R, x_diffraction_pt, title = "The optimized layout", limits = limits)
